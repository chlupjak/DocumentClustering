<%-- 
    Document   : document_adding
    Created on : 6.4.2016, 14:08:43
    Author     : jakub
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Document clustering</title>
    </head>
    <body>
        <form action="documentProcessor" method="POST">
            Document name: <input type="text" name="document_name"><br />
            Author: <input type="text" name="document_author"/> <br>
            Text: <br><textarea name="document_text" cols="150" rows="30"></textarea> <br>
            <input type="submit" value="Submit" />
        </form>
    </body>
</html>

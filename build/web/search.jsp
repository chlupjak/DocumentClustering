<%-- 
    Document   : search
    Created on : 7.4.2016, 17:06:52
    Author     : jakub
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Document cluster</title>
    </head>
    <body>
        <h1>Document cluster</h1>
        
        <div >
            <a href="index.jsp">Home</a>
            <a href="documents.jsp">All documents</a>
            <a href="cluster_documents.jsp?id=0">Cluster 1</a>
            <a href="cluster_documents.jsp?id=1">Cluster 2</a>
            <a href="cluster_documents.jsp?id=2">Cluster 3</a>
            <a href="document_adding.jsp">Add document</a>
            <a href="search.jsp">Search</a>
        </div>
        
        <form action="search" method="POST">
            Text: <input type="text" name="text">* <input type="submit" value="Search" /><br />
            Document author: <input type="text" name="document_author"/> <br>
            Document name: <input type="text" name="document_name"/> <br>
            <input type="radio" name="in" value="key" checked="checked"> keywords 
            <input type="radio" name="in" value="entities"> named entities 
            <input type="radio" name="in" value="all"> whole text <br><br>
            <input type="radio" name="cluster" value="all" checked="checked"> All documents
            <input type="radio" name="cluster" value="0"> Cluster 1 
            <input type="radio" name="cluster" value="1"> Cluster 2
            <input type="radio" name="cluster" value="2"> Cluster 3
        </form>
        
        
    </body>
</html>

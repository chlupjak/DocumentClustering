/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import alchemyAPIClient.AlchemyAPIClient;
import cluster.Cluster;
import cluster.Document;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jakub
 */
public class DocumentProcessor extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String doc_name = request.getParameter("document_name");
        String doc_author = request.getParameter("document_author");
        String doc_text = request.getParameter("document_text");

        Document doc = new Document(doc_name, doc_text, doc_author);
        Cluster.getInstance().addDocument(doc);

        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Document Processor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>New document added</h1>");
            DocumentServlet.printNavigation(out);
            out.println("<p>" + doc_author + " - " + doc_name + "</p>");
            out.println("<p>" + doc.printKeywords() + "</p>");
            out.println("<p>" + doc.printNamedEntities() + "</p>");
            out.println("<p>" + doc_text + "</p>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import cluster.Cluster;
import cluster.Document;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jakub
 */
public class DocumentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String id = request.getParameter("id");
            String c = request.getParameter("cluster");
            Document document;
            if (c == null){
                document = Cluster.getInstance().getDocumentByID(Integer.valueOf(id));
            } else {
                document = Cluster.getInstance().getDocumentByClusterAndID(Integer.valueOf(c), Integer.valueOf(id));
            }
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Document " + document.getName() +"</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>" + document.getAuthor() + " - " + document.getName() + "</h1>");
            printNavigation(out);
            out.println("<p>" + document.printKeywords() + "</p>");
            out.println("<p>" + document.printNamedEntities() + "</p>");
            out.println("<p>" + document.getText() + "</p>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    public static void printNavigation(PrintWriter out) {
        out.println("<div ><a href=\"index.jsp\">Home</a>");
        out.println("<a href=\"documents.jsp\">All documents</a>");
        out.println("<a href=\"cluster_documents.jsp?id=0\">Cluster 1</a>");
        out.println("<a href=\"cluster_documents.jsp?id=1\">Cluster 2</a>");
        out.println("<a href=\"cluster_documents.jsp?id=2\">Cluster 3</a>");
        out.println("<a href=\"document_adding.jsp\">Add document</a>");
        out.println("<a href=\"search.jsp\">Search</a></div>");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

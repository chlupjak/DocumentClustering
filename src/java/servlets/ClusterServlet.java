/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import cluster.Cluster;
import cluster.Document;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jakub
 */
public class ClusterServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String method = request.getParameter("method");
            switch (method) {
                case "allDoc":
                    printAllDoc(out);
                    break;
                case "clusterList":
                    printClusterList(out);
                    break;
                case "organize":
                    Cluster.getInstance().processClustering();
                    printClusterList(out);
                    break;
                case "clusterData":
                    printCluster(Integer.valueOf(request.getParameter("id")), out);
                    break;
            }
            out.flush();
            out.close();
        }
    }

    private void printAllDoc(PrintWriter out) {
        Cluster cluster = Cluster.getInstance();
        for (Document doc : cluster.getAllDocuments()) {
            out.println("<a href=\"document?id=" + doc.getId() + "\">" + doc.getName() + " - " + doc.getAuthor() + "</a><br>");
        }
    }

    private void printClusterList(PrintWriter out) {
        Cluster cluster = Cluster.getInstance();
        if (cluster.isClustered()) {
            out.println("<table border=\"1\" style=\"width:80%\"><tr><th>Cluster 1</th><th>Cluster 2</th><th>Cluster 3</th></tr>");
            out.println("<tr><td>" + cluster.getKeyWordsOfCluster(0) + "</td>");
            out.println("<td>" + cluster.getKeyWordsOfCluster(1) + "</td>");
            out.println("<td>" + cluster.getKeyWordsOfCluster(2) + "</td></tr><tr>");
            for (int i = 0; i < 3; i++) {
                out.println("<td><ul>");
                for (Document doc : cluster.getDocumentsInCluster(i)) {
                    out.println("<li>" + "<a href=\"document?id=" + doc.getId() + "&cluster=" + i + "\">" + doc.getName() + " - " + doc.getAuthor() + "</a></li>");
                }
                out.println("</ul></td>");
            }
            out.println("</tr></table>");
            return;
        }
        printNotClustered(out);
    }

    private void printNotClustered(PrintWriter out) {
        out.println("<h2>Documents are't arranged in clusters</h2>");
        out.println("<a href=\"cluster?method=organize\">organize documents now</a>");
        out.println("<h3>All documents:</h3>");
        printAllDoc(out);
    }

    private void printCluster(int id, PrintWriter out) {
        Cluster cluster = Cluster.getInstance();
        if (cluster.isClustered()) {
            out.print("<h2>Cluster " + (id + 1) + "</h2>");
            out.print("<h3>Keywords</h3>");
            out.println("<p>" + cluster.getKeyWordsOfCluster(id) + "</p>");
            out.println("<ul>");
            for (Document doc : cluster.getDocumentsInCluster(id)) {
                out.println("<li>" + "<a href=\"document?id=" + doc.getId() + "&cluster=" + id + "\">" + doc.getName() + " - " + doc.getAuthor() + "</a></li>");
            }
            out.println("</ul>");
            return;
        }
        printNotClustered(out);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

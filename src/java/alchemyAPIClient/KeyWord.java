/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alchemyAPIClient;

import org.w3c.dom.Node;

/**
 *
 * @author jakub
 */
public class KeyWord {
    
    private String text;
    private String relevance;
    
    public KeyWord(Node keyword){
        for (int i = 0; i < keyword.getChildNodes().getLength(); i++) {
            Node node = keyword.getChildNodes().item(i);
            String name = node.getNodeName();
            switch (name) {
                case "text":
                    text = node.getTextContent();
                    break;
                case "relevance":
                    relevance = node.getTextContent();
                    break;
            }
        }
    }

    public String getText() {
        return text;
    }

    public double getRelevance() {
        return Double.valueOf(relevance);
    }
    
    public String toString(){
        return text;
    }
}

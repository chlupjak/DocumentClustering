/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alchemyAPIClient;

import org.w3c.dom.Node;

/**
 *
 * @author jakub
 */
public class NamedEntity {

    private String text;
    private String type;
    private String relevance;
    private String count;

    public NamedEntity(Node entity) {
        for (int i = 0; i < entity.getChildNodes().getLength(); i++) {
            Node node = entity.getChildNodes().item(i);
            String name = node.getNodeName();
            switch (name) {
                case "text":
                    text = node.getTextContent();
                    break;
                case "type":
                    type = node.getTextContent();
                    break;
                case "relevance":
                    relevance = node.getTextContent();
                    break;
                case "count":
                    count = node.getTextContent();
                    break;
            }
        }
    }

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }

    public String getRelevance() {
        return relevance;
    }

    public int getCount() {
        return Integer.valueOf(count);
    }

}

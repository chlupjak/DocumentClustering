/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alchemyAPIClient;

import com.alchemyapi.api.AlchemyAPI;
import com.alchemyapi.api.AlchemyAPI_KeywordParams;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jakub
 */
public class AlchemyAPIClient {
    
    private static AlchemyAPIClient instance;
    private AlchemyAPI alchemyAPI;
    
    private AlchemyAPIClient(String apiKey){
        alchemyAPI = AlchemyAPI.GetInstanceFromString(apiKey);
    }
    
    public static AlchemyAPIClient getInstance(String apiKey){
        if (instance == null){
            instance = new AlchemyAPIClient(apiKey);
        }
        return instance;
    }
    
    private String getStringFromDocumnet(Document document){
        try { 
            DOMSource domSource = new DOMSource(document); 
            StringWriter writer = new StringWriter(); 
            StreamResult result = new StreamResult(writer); 
 
            TransformerFactory tf = TransformerFactory.newInstance(); 
            Transformer transformer = tf.newTransformer(); 
            transformer.transform(domSource, result); 
 
            return writer.toString(); 
        } catch (TransformerException ex) { 
            ex.printStackTrace(); 
            return null; 
        } 
    }
    
    public void processDocument(String text){
        try {
            Document document = alchemyAPI.TextGetRankedKeywords(text);
            
        } catch (IOException | SAXException | ParserConfigurationException | XPathExpressionException ex) {
            Logger.getLogger(AlchemyAPIClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
//    NodeList nl = n.getChildNodes().item(0).getChildNodes();
//            for (int i = 0; i < nl.getLength(); i++){
//                org.w3c.dom.Node newNode = nl.item(i);
//                if ("entities".equals(newNode.getNodeName())){
//                    for (int j = 0; j < newNode.getChildNodes().getLength(); j++){
//                        org.w3c.dom.Node entity = newNode.getChildNodes().item(j);
//                        String entName = entity.getLocalName();
//                        entName = entity.getNodeName();
//                        entName = entity.getNodeValue();
//                        for (int k = 0; k < entity.getChildNodes().getLength(); k++){
//                            String na = entity.getChildNodes().item(k).getNodeName();
//                            String tex = entity.getChildNodes().item(k).getTextContent();
//                            String t = entity.getChildNodes().item(k).getNodeValue();
//                            switch (na){}}}
    
    
    public ArrayList<NamedEntity> getEntities(String text){
        ArrayList<NamedEntity> result = null;
        try {
            result = new ArrayList<>();
            Document doc = alchemyAPI.TextGetRankedNamedEntities(text);
            DOMSource domSource = new DOMSource(doc);
            NodeList nodeList = domSource.getNode().getChildNodes().item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++){
                Node newNode = nodeList.item(i);
                if ("entities".equals(newNode.getNodeName())){
                    for (int j = 0; j < newNode.getChildNodes().getLength(); j++){
                        org.w3c.dom.Node entity = newNode.getChildNodes().item(j);
                        if ("entity".equals(entity.getNodeName())){
                            result.add(new NamedEntity(entity));
                        }
                    }
                }
            }
        } catch (IOException | SAXException | ParserConfigurationException | XPathExpressionException ex) {
            Logger.getLogger(AlchemyAPIClient.class.getName()).log(Level.SEVERE, null, ex);
            return result;
        }
        return result;
    }
    
    public ArrayList<KeyWord> getKeyWords(String text){
        ArrayList<KeyWord> result = null;
        try {
            result = new ArrayList<>();
            Document doc = alchemyAPI.TextGetRankedKeywords(text, new AlchemyAPI_KeywordParams());
            DOMSource domSource = new DOMSource(doc);
            NodeList nodeList = domSource.getNode().getChildNodes().item(0).getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++){
                Node newNode = nodeList.item(i);
                if ("keywords".equals(newNode.getNodeName())){
                    for (int j = 0; j < newNode.getChildNodes().getLength(); j++){
                        org.w3c.dom.Node entity = newNode.getChildNodes().item(j);
                        if ("keyword".equals(entity.getNodeName())){
                            result.add(new KeyWord(entity));
                        }
                    }
                }
            } 
        } catch (Exception ex) {
            Logger.getLogger(AlchemyAPIClient.class.getName()).log(Level.SEVERE, null, ex);
            return result;
        }
        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package documentProcessor;

import alchemyAPIClient.AlchemyAPIClient;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jakub
 */

public class DocumentProcessor extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String doc_name = request.getParameter("document_name");
        String doc_author = request.getParameter("document_author");
        String doc_text = request.getParameter("document_text");
        
        AlchemyAPIClient alchemyAPIClient = AlchemyAPIClient.getInstance("885256d51f8f4aca2dd5144782a12a273f335cee");
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Document Processor</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>New document added</h1>");
            out.println("<p>" + doc_author + " - " + doc_name + "</p>");
            out.println("<p>" + AlchemyAPIClient.printNamedEntities(alchemyAPIClient.getEntities(doc_text)) + "</p>");
            out.println("<p>" + AlchemyAPIClient.printKeywords(alchemyAPIClient.getKeyWords(doc_text)) + "</p>");
            out.println("<p>" + doc_text + "</p>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluster;

import alchemyAPIClient.AlchemyAPIClient;
import alchemyAPIClient.KeyWord;
import alchemyAPIClient.NamedEntity;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author jakub
 */
public class Document {

    private String name;
    private String text;
    private String author;
    private int id = 0;
    private ArrayList<NamedEntity> namedEntities;
    private ArrayList<KeyWord> keyWords;

    public Document(String name, String text, String author) {
        this.name = name;
        this.text = text;
        this.author = author;
        processText();
    }
    
    public void setId(int id){
        this.id = id;
    }

    private void processText() {
        AlchemyAPIClient alchemyAPIClient = AlchemyAPIClient.getInstance("885256d51f8f4aca2dd5144782a12a273f335cee");
        namedEntities = alchemyAPIClient.getEntities(text);
        keyWords = alchemyAPIClient.getKeyWords(text);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }
    
    public String getAuthor(){
        return author;
    }

    public ArrayList<NamedEntity> getNamedEntities() {
        return namedEntities;
    }

    public ArrayList<KeyWord> getKeyWords() {
        return keyWords;
    }

    public double tf(String term) {
        double n = 0;
        double relevance = 0;
        for (NamedEntity namedEntity : namedEntities) {
            if (term.equalsIgnoreCase(namedEntity.getText())) {
                n += namedEntity.getCount();
                relevance = Double.valueOf(namedEntity.getRelevance());
            }
        }
        for (KeyWord keyWord : keyWords) {
            if (term.equalsIgnoreCase(keyWord.getText())) {
                n++;
                double r = Double.valueOf(keyWord.getRelevance());
                if (r > relevance){
                    relevance = r;
                }
            }
        }
        return (n / (namedEntities.size() + keyWords.size())) * relevance;
    }
    
    public String printNamedEntities(){
        StringBuilder builder = new StringBuilder("<table><tr><th>Type</th><th>Relevance</th><th>Text</th><th>Count</th></tr>");
        for (NamedEntity entity: namedEntities){
            builder.append("<tr><td>" + entity.getType() + "</td>");
            builder.append("<td>" + entity.getRelevance() + "</td>");
            builder.append("<td>" + entity.getText()+ "</td>");
            builder.append("<td>" + entity.getCount()+ "</td></tr>");
        }
        builder.append("</table>");
        return builder.toString();
    }
    
    public String printKeywords(){
        StringBuilder builder = new StringBuilder("<table><tr><th>Relevance</th><th>Text</th></tr>");
        for (KeyWord keyWord: keyWords){
            builder.append("<tr><td>" + keyWord.getRelevance() + "</td>");
            builder.append("<td>" + keyWord.getText()+ "</td></tr>");
        }
        builder.append("</table>");
        return builder.toString();
    }
    
    public String getKeyWord(){
        String result = null;
        double relevance = 0;
        for (KeyWord keyWord: keyWords){
            if (relevance < keyWord.getRelevance()){
                relevance = keyWord.getRelevance();
                result = keyWord.getText();
            }
        }
        return result;
    }
    
    public boolean searchKeyWord(String word){
        for (KeyWord keyWord: keyWords){
            if (word.equalsIgnoreCase(keyWord.getText())){
                return true;
            }
        }
        return false;
    }
    
    public boolean searchNamedEntity(String word){
        for (NamedEntity namedEntity: namedEntities){
            if (word.equalsIgnoreCase(namedEntity.getText())){
                return true;
            }
        }
        return false;
    }
    
    public boolean searchInText(String word){
        String [] words = text.replaceAll("[^a-zA-Z0-9]+","").split(" ");
        for (String w: words){
            if (w.equalsIgnoreCase(word)){
                return true;
            }
        }
        return false;
    }

}

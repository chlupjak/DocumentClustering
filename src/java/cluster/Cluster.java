/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cluster;

import alchemyAPIClient.KeyWord;
import alchemyAPIClient.NamedEntity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 *
 * @author jakub
 */
public class Cluster {

    private static Cluster instance;

    private ArrayList<Document> docs = new ArrayList<>();
    private ArrayList<String> global = new ArrayList<>();

    private ArrayList<HashMap<Integer, Integer>> cluster;

    private boolean clustered = false;

    private Cluster() {

    }

    public static Cluster getInstance() {
        if (instance == null) {
            instance = new Cluster();
        }
        return instance;
    }

    private double idf(String term) {
        double n = 0;
        for (Document doc : docs) {
            n += doc.tf(term);
        }
        return Math.log(docs.size() / n);
    }

    private static double cosSim(double[] a, double[] b) {
        double dotp = 0, maga = 0, magb = 0;
        for (int i = 0; i < a.length; i++) {
            dotp += a[i] * b[i];
            maga += Math.pow(a[i], 2);
            magb += Math.pow(b[i], 2);
        }
        maga = Math.sqrt(maga);
        magb = Math.sqrt(magb);
        double d = dotp / (maga * magb);
        return d == Double.NaN ? 0 : d;
    }

    public void processClustering() {
        //compute tf-idf and create document vectors (double[])
        clustered = true;
        ArrayList<double[]> vecspace = new ArrayList<double[]>();
        for (Document doc : docs) {
            double[] d = new double[global.size()];
            for (int i = 0; i < global.size(); i++) {
                d[i] = doc.tf(global.get(i)) * idf(global.get(i));
            }
            vecspace.add(d);
        }
        //iterate k-means
        HashMap<double[], TreeSet<Integer>> clusters = new HashMap<>();
        HashMap<double[], TreeSet<Integer>> step = new HashMap<>();
        HashSet<Integer> rand = new HashSet<>();
        TreeMap<Double, HashMap<double[], TreeSet<Integer>>> errorsums = new TreeMap<>();
        int k = 3;
        int maxiter = 500;
        for (int init = 0; init < 100; init++) {
            clusters.clear();
            step.clear();
            rand.clear();
            //randomly initialize cluster centers
            while (rand.size() < k) {
                rand.add((int) (Math.random() * vecspace.size()));
            }
            for (int r : rand) {
                double[] temp = new double[vecspace.get(r).length];
                System.arraycopy(vecspace.get(r), 0, temp, 0, temp.length);
                step.put(temp, new TreeSet<Integer>());
            }
            boolean go = true;
            int iter = 0;
            while (go) {
                clusters = new HashMap<>(step);
                //cluster assignment step
                for (int i = 0; i < vecspace.size(); i++) {
                    double[] cent = null;
                    double sim = 0;
                    for (double[] c : clusters.keySet()) {
                        double csim = cosSim(vecspace.get(i), c);
                        if (csim > sim) {
                            sim = csim;
                            cent = c;
                        }
                    }
                    if (clusters.containsKey(cent)){
                        clusters.get(cent).add(i);
                    }  
                }
                //centroid update step
                step.clear();
                for (double[] cent : clusters.keySet()) {
                    double[] updatec = new double[cent.length];
                    for (int d : clusters.get(cent)) {
                        double[] doc = vecspace.get(d);
                        for (int i = 0; i < updatec.length; i++) {
                            updatec[i] += doc[i];
                        }
                    }
                    for (int i = 0; i < updatec.length; i++) {
                        updatec[i] /= clusters.get(cent).size();
                    }
                    step.put(updatec, new TreeSet<Integer>());
                }
                //check break conditions
                String oldcent = "", newcent = "";
                for (double[] x : clusters.keySet()) {
                    oldcent += Arrays.toString(x);
                }
                for (double[] x : step.keySet()) {
                    newcent += Arrays.toString(x);
                }
                if (oldcent.equals(newcent)) {
                    go = false;
                }
                if (++iter >= maxiter) {
                    go = false;
                }
            }
            //calculate similarity sum and map it to the clustering
            double sumsim = 0;
            for (double[] c : clusters.keySet()) {
                TreeSet<Integer> cl = clusters.get(c);
                for (int vi : cl) {
                    sumsim += cosSim(c, vecspace.get(vi));
                }
            }
            errorsums.put(sumsim, new HashMap<>(clusters));
        }
        this.cluster = new ArrayList<>();
        for (double[] cent : errorsums.get(errorsums.lastKey()).keySet()) {
            HashMap<Integer, Integer> newCluster = new HashMap<>();
            for (int pts : errorsums.get(errorsums.lastKey()).get(cent)) {
                newCluster.put(docs.get(pts).getId(), pts);
            }
            this.cluster.add(newCluster);
        }
    }

    public void addDocument(Document doc) {
        if (docs.size() > 0) {
            doc.setId(docs.get(docs.size() - 1).getId() + 1);
        }
        docs.add(doc);
        for (KeyWord word : doc.getKeyWords()) {
            global.add(word.getText());
        }
        for (NamedEntity entity : doc.getNamedEntities()) {
            global.add(entity.getText());
        }
        clustered = false;
    }

    public boolean isClustered() {
        return clustered;
    }

    public Document getDocumentByID(int id) {
        for (Document doc : docs) {
            if (doc.getId() == id) {
                return doc;
            }
        }
        return null;
    }

    public Document getDocumentByClusterAndID(int c, int id) {
        if (cluster != null && cluster.size() > c) {
            return docs.get(cluster.get(c).get(id));
        }
        return getDocumentByID(id);
    }

    public ArrayList<Document> getDocumentsInCluster(int clusterNumber) {
        ArrayList<Document> result = new ArrayList<>();
        if (cluster != null && cluster.size() > clusterNumber) {
            for (int i : cluster.get(clusterNumber).values()) {
                result.add(docs.get(i));
            }
        }
        return result;
    }

    public ArrayList<Document> getAllDocuments() {
        return docs;
    }

    public String getKeyWordsOfCluster(int clusterNumber) {
        String result = "";
        for (Document doc : getDocumentsInCluster(clusterNumber)) {
            result += doc.getKeyWord() + ", ";
        }
        return result;
    }

    public ArrayList<Document> search(String text, String author, String name, String in, String cl) {
        ArrayList<Document> documents;
        switch (cl) {
            case "all":
                documents = docs;
                break;
            default:
                documents = getDocumentsInCluster(Integer.valueOf(cl));
                break;
        }
        if (author != null && !author.equals("")) {
            documents = searchAuthor(documents, author);
        }
        if (name != null && !name.equals("")) {
            documents = searchName(documents, name);
        }
        switch (in) {
            case "key":
                documents = searchKeyWord(documents, text);
                break;
            case "entities":
                documents = searchNamedEntities(documents, text);
                break;
            default:
                documents = searchInText(documents, text);
                break;
        }
        return documents;
    }

    private ArrayList<Document> searchAuthor(ArrayList<Document> documents, String author) {
        ArrayList<Document> result = new ArrayList<>();
        for (Document document : documents) {
            if (author.equalsIgnoreCase(document.getAuthor())) {
                result.add(document);
            }
        }
        return result;
    }

    private ArrayList<Document> searchName(ArrayList<Document> documents, String name) {
        ArrayList<Document> result = new ArrayList<>();
        for (Document document : documents) {
            if (name.equalsIgnoreCase(document.getName())) {
                result.add(document);
            }
        }
        return result;
    }

    private ArrayList<Document> searchKeyWord(ArrayList<Document> documents, String text) {
        ArrayList<Document> result = new ArrayList<>();
        for (Document document : documents) {
            if (document.searchKeyWord(text)) {
                result.add(document);
            }
        }
        return result;
    }

    private ArrayList<Document> searchNamedEntities(ArrayList<Document> documents, String text) {
        ArrayList<Document> result = new ArrayList<>();
        for (Document document : documents) {
            if (document.searchNamedEntity(text)) {
                result.add(document);
            }
        }
        return result;
    }

    private ArrayList<Document> searchInText(ArrayList<Document> documents, String text) {
        ArrayList<Document> result = new ArrayList<>();
        for (Document document : documents) {
            if (document.searchInText(text)) {
                result.add(document);
            }
        }
        return result;
    }

}

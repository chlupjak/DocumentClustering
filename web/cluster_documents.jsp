<%-- 
    Document   : cluster_documents
    Created on : 7.4.2016, 17:36:04
    Author     : jakub
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Document cluster</title>
    </head>
    <body>
        <h1> Document cluster </h1>

        <div >
            <a href="index.jsp">Home</a>
            <a href="documents.jsp">All documents</a>
            <a href="cluster_documents.jsp?id=0">Cluster 1</a>
            <a href="cluster_documents.jsp?id=1">Cluster 2</a>
            <a href="cluster_documents.jsp?id=2">Cluster 3</a>
            <a href="document_adding.jsp">Add document</a>
            <a href="search.jsp">Search</a>
        </div>
    
        <jsp:include page="cluster" flush="true">
            <jsp:param name="method" value="clusterData" />
            <jsp:param name="id" value="${param.id}" />
        </jsp:include>
    </body>
</html>
